require 'test_helper'

class DetectorControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get detector_index_url
    assert_response :success
  end

  test "should get summary" do
    get detector_summary_url
    assert_response :success
  end

end
