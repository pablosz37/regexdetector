class DetectorController < ApplicationController
  def index
  end

  def summary
    regexes = params[:regex]
    text = params[:text]
    @sum = Hash.new
    regexes.split.each do |regex|
      @sum[regex] = text.scan(Regexp.new(regex)).size 
    end
  end
end
